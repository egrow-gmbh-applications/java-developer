package io.egrow.job_application.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
public class CategoriesController {

    private Set<String> cleanCategories(List<String> rawCategories) {
        return rawCategories
                .stream()
                .filter(Objects::nonNull)
                .filter(category -> !category.isEmpty())
                .filter(category -> 10 > category.length())
                .collect(Collectors.toSet());
    }

    CategoriesService categoriesService;

    private static final String CATEGORIES_PREFIX = "cat_";

    @GetMapping("/categories")
    public ResponseEntity<Set<String>> get() {

        List<String> allCategories = this.categoriesService.getAll();

        Set<String> cleanedCategories = this.cleanCategories(allCategories);

        Set<String> categoriesWithPrefix = cleanedCategories
                .stream()
                .map(category -> CATEGORIES_PREFIX + category)
                .collect(Collectors.toSet());

        return new ResponseEntity<>(categoriesWithPrefix, HttpStatus.OK);
    }

    public CategoriesController(CategoriesService categoriesService) {
        this.categoriesService = categoriesService;
    }
}
