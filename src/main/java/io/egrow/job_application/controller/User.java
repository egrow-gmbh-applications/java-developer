package io.egrow.job_application.controller;

import io.egrow.job_application.entity.Userid;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class User {

    Userid user_id;
    String userName;
    String userGoal;

    public User(String user_id, String userName, String userGoal) {
        this.user_id = new Userid(Integer.parseInt(user_id));
        this.userName = userName;
        this.userGoal = userGoal;
    }

    public Userid getid() {
        return user_id;
    }

    public String getname() {
        return userName;
    }

    public String getUserGoal() {
        return userGoal;
    }

    public String transformationJSONString() {
        return "{" +
                "'id': '" + user_id.num_value() + "'," +
                "'name': '" + userName + "'," +
                "'goal': '" + userGoal + "'," +
                "}";
    }
}
